" Disable go/vim version warning
let g:go_version_warning = 0

if file_readable('/home/' . $SUDO_USER . '/.vimrc')
    set runtimepath+=/home/$SUDO_USER/.vim_runtime

    source /home/$SUDO_USER/.vim_runtime/vimrcs/basic.vim
    source /home/$SUDO_USER/.vim_runtime/vimrcs/filetypes.vim
    source /home/$SUDO_USER/.vim_runtime/vimrcs/plugins_config.vim
    source /home/$SUDO_USER/.vim_runtime/vimrcs/extended.vim

    try
    source /home/$SUDO_USER/.dotfiles/my_configs.vim
    catch
    endtry

else
    set runtimepath+=~/.vim_runtime

    source ~/.vim_runtime/vimrcs/basic.vim
    source ~/.vim_runtime/vimrcs/filetypes.vim
    source ~/.vim_runtime/vimrcs/plugins_config.vim
    source ~/.vim_runtime/vimrcs/extended.vim

    try
    source ~/.dotfiles/my_configs.vim
    catch
    endtry
endif
