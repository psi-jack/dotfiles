call pathogen#infect('~/.dotfiles/vim_plugins/{}')

" Enable mouse support
set mouse=a

" Use X clipboard
set clipboard=unnamedplus

" Close NERDTree on Open
let NERDTreeQuitOnOpen=1

" Alternative tab controls
map <C-q><left> :tabp<cr>
map <C-q><right> :tabn<cr>
map <C-q><up> :tabr<cr>
map <C-q><down> :tabl<cr>

" Unmap the Insert Mode Mappings I don't like:
iunmap $1
iunmap $2
iunmap $3
iunmap $4
iunmap $q
iunmap $e
iunmap $t

" Tune EditorConfig:
let g:EditorConfig_exclude_patterns = ['fugitive://.*', 'scp://.*']

