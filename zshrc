#if [[ -f /etc/solus-release ]]; then
#    if [[ $TILIX_ID || $VTE_VERSION ]]; then
#        echo "Loading VTE extensions..."
#        source /usr/share/defaults/etc/profile.d/vte.sh
#    fi
#fi
#precmd_functions+=(__vte_osc7)


#
# User configuration sourced by interactive shells
#
typeset -U PATH
export PATH

# Change default zim location
export ZIM_HOME=${ZDOTDIR:-${HOME}}/.zim

# Start zim
[[ -s ${ZIM_HOME}/init.zsh ]] && source ${ZIM_HOME}/init.zsh

# Fix the ESC+Backspace
bindkey -e
bindkey -e "^[[1;5C" forward-word
bindkey -e "^[[1;5D" backward-word

# Default Environment
export EDITOR=vim
export VISUAL=vim
export PAGER=less
export BORG_REPO=/mnt/backups/psi-jack/borg

#function custom_prompt() {
#    VTE_PWD_THING="$(__vte_osc7)"
#    PS1="$PS1$VTE_PWD_THING"
#}

if [[ -f /etc/solus-release ]]; then
    export SSH_ASKPASS=/usr/lib64/seahorse/seahorse/ssh-askpass

    if [[ $TILIX_ID || $VTE_VERSION ]]; then
        source /usr/share/defaults/etc/profile.d/vte.sh
        precmd_functions+=(__vte_osc7)
    fi
fi

# Set GPG TTY
#export GPG_TTY=$(tty)

# Refresh gpg-agent tty in case user switches into an X session
#gpg-connect-agent updatestartuptty /bye >/dev/null

# Update SSH Agent Socket
#unset SSH_AGENT_PID
#if [[ "${gnupg_SSH_AUTH_SOCK_by:-0}" -ne $$ ]]; then
#  export SSH_AUTH_SOCK="$(gpgconf --list-dirs agent-ssh-socket)"
#fi

# Disable Ctrl+S Scroll-Locking
stty -ixon

# Include local functions/aliases/environments:
while read f
do
    source "$f"
done < <(find "${HOME}/.local/dotfiles/" -type f -name '*.zsh')

